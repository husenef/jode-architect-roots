/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can 
 * always reference jQuery with $, even when in .noConflict() mode.
 *
 * Google CDN, Latest jQuery
 * To use the default WordPress version of jQuery, go to lib/config.php and
 * remove or comment out: add_theme_support('jquery-cdn');
 * ======================================================================== */

(function ($) {

// Use this variable to set up the common and page specific functions. If you 
// rename this variable, you will also need to rename the namespace below.
    var windowsw = $(window).width();
    var Roots = {
        // All pages
        common: {
            init: function () {
                // JavaScript to be fired on all pages
//                $(".banner").sticky({bottomSpacing: 0});
                $("#main-menu > li.dropdown").on('click', function () {
                    if (windowsw > 767) {
                        if ($(this).hasClass('open')) {
                            $(this).parents('.container-fluid').animate({'min-height': '50'}, 500, function () {
                                $(this).find('a.navbar-brand').animate({height: '80'}, 500);
//                                $(this).find('ul#main-menu > li').animate({minHeight: '50'}, 500);
                                $("#main-menu ul.dropdown-menu").data('owlCarousel').destroy();
                            });
                        } else {
                            $(this).parents('.container-fluid').animate({'min-height': 150}, 500, function () {
                                $(this).find('a.navbar-brand').animate({height: '150'}, 500);
                                $(this).find('ul#main-menu > li').animate({minHeight: '50'}, 500);
                                run_owl();
                            });
                        }
                    } else {
                        if ($(this).hasClass('open')) {
                            $("#main-menu ul.dropdown-menu").data('owlCarousel').destroy();
                        } else {
                            run_owl();
                        }

//                        $('#main-menu').on('hide.bs.dropdown', function () {
//                            $(this).parents('.banner').animate({'min-height': '50'}, 500);
//                        });
//                        $('#main-menu').on('show.bs.dropdown', function () {
//                            $(this).parents('.banner').animate({'min-height': 150}, 500);
//                        });
                    }
                });
                if (windowsw < 768) {
                    $('li.active').find('ul.dropdown-menu').owlCarousel({
                        items: 4,
                        navigation: false,
                        paginationNumbers: false,
                        itemsTablet: [768, 3],
                        itemsMobile: [400, 2]
                    });
                }
                function run_owl() {
                    $('#main-menu ul.dropdown-menu').owlCarousel({
                        items: 4,
                        navigation: false,
                        paginationNumbers: false,
                        itemsTablet: [768, 3],
                        itemsMobile: [400, 2]
                    });
                }

                $('.full').on('click', function () {
                    var h = $('.banner').height();
                    var btn = $(this);
                    if (btn.hasClass('open')) {
                        btn.removeClass('open');
                        $('.banner').animate({bottom: "-" + h + "px"}, 'slow', function () {
                            $(this).find('.full i').removeClass('fa-angle-double-down').addClass('fa-angle-double-up').attr('title', 'Show Menu');
                        });
                    } else {
                        btn.addClass('open');
                        $('.banner').animate({bottom: 'none'}, 'slow', function () {
                            $(this).find('.full i').removeClass('fa-angle-double-up').addClass('fa-angle-double-down').attr('title', 'Close Menu');
                        });
                    }
                });
            }
        },
        // Home page
        home: {
            init: function () {
                // JavaScript to be fired on the home page
            }
        },
        // About us page, note the change from about-us to about_us.
        about_us: {
            init: function () {
                // JavaScript to be fired on the about us page
            }
        }
    };

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function (func, funcname, args) {
            var namespace = Roots;
            funcname = (funcname === undefined) ? 'init' : funcname;
            if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function () {
            UTIL.fire('common');

            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
                UTIL.fire(classnm);
            });
        }
    };

    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
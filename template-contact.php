<?php
/*
  Template Name: Contact Template
 */
?>
<div class="row">
    <?php while (have_posts()) : the_post(); ?>
        <div class="col-lg-12 title">
            <h3><?php the_title() ?></h3>
        </div>
        <div class="col-lg-3">
            <?php the_content(); ?>
        </div>
        <div class="col-lg-7 col-lg-push-1 bordered">
            <div class="row">
                <div class="col-md-push-2 col-md-8">
                    <?php
                    echo do_shortcode(do_shortcode("[get_form]"));
                    ?>
                </div>
            </div>
        </div>
    <?php endwhile; ?>

</div>
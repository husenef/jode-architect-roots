<header class="banner navbar navbar-default" role="banner">
    <button class="full open" title="Close Menu">
        <i class="fa-angle-double-down fa"></i>
    </button>
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
                <img src="<?php echo esc_url(get_template_directory_uri()) ?>/assets/img/logo.jpg" alt="<?php bloginfo('name'); ?>" class="img-responsive"/>
            </a>
        </div>

        <nav class="collapse navbar-collapse" role="navigation">
            <?php
            if (has_nav_menu('primary_navigation')) :
                wp_nav_menu(array('theme_location' => 'primary_navigation', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'nav navbar-nav','menu_id'=>'main-menu'));
            endif;
            ?>
        </nav>
    </div>
</header>

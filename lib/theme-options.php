<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('ABSPATH')) {
    exit; // disable direct access
}
if (!class_exists('Site_Options')) :

    class Site_Options {

        var $siteOptions = null;
        var $optionName = 'jodeOptions';
        var $options = array();

        public static function init() {
            $siteOptions = new self();
        }

        function __construct() {

            $this->saveOptions();
            $this->setOptions();
            $this->pages();
        }

        function pages() {
            $html = "";
            $html .= "<h3>Site Options</h3>";
            $html .= "<form action='' method='post'>";
            $html .="<table class='form-table'>";
            $html .="<tbody><tr>";
            $html .="<th scope='row'><label for='form'>Contact Form 7</label></th>";
            $html .="<td><input type='text' name='form7' value='" . stripslashes($this->options->form7) . "' placeholder='Kode contact form 7' /></td>";
            $html .="</tr></tbody></table>";
            $html .="<p class='submit'><input type='submit' value='Update Options' class='button button-primary' id='submit'></p> ";
            $html .="</form>";
            echo $html;
        }

        function saveOptions() {
            if ($_POST):
                $data = json_encode($_POST);
                if (get_option($this->optionName) !== false) {
                    update_option($this->optionName, $data);
                } else {
                    $deprecated = null;
                    $autoload = 'no';
                    add_option($this->optionName, $data, $deprecated, $autoload);
                }
                $_SESSION['message'] = array('c' => '1', 'm' => 'Data Sudah di simpan');
                $this->redirect(admin_url('themes.php?page=theme_option.php'));
            endif;
        }

        function setOptions() {
            $options = get_option($this->optionName);
            $this->options = json_decode($options);
            return $this;
        }

        function redirect($url) {
            echo "<script type='text/javascript'>window.location = '" . $url . "';</script>";
        }

        

    }

    endif;

function themeoption() {

    //add_theme_page( $page_title, $menu_title, $capability, $menu_slug, $function = '' )
    add_theme_page('Site Option Jode Architects', 'Jode Architects Site Options', 'manage_options', 'theme_option.php', array('Site_Options', 'init'));
}

add_action('admin_menu', 'themeoption');


<?php

/**
 * Clean up the_excerpt()
 */
function roots_excerpt_more() {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'roots') . '</a>';
}

add_filter('excerpt_more', 'roots_excerpt_more');

add_shortcode('get_form', generate_shortcode);

function generate_shortcode($attr, $content) {
    $options = get_option('jodeOptions');
    $op = json_decode($options);
    $return = $op->form7;

    return stripslashes($return);
}
